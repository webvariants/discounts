jQuery(function($) {

	$('#reusable').on('change', function () {
		$('#attendance').val(0);
		$('#attendance').prop('disabled', !$('#attendance').prop('disabled'));

		$('#code').parents('.sly-form-row').toggleClass('sly-hidden');
	});

	// toggle tree view
	$('#wv-discounts-campaign-products')
		.on('open close', '.toggle', function(e) {
			var open = e.type === 'open' ? true : false;

			$(this).toggleClass('closed', !open);
			if (open) {
				$(this).parent().find('> ul').slideDown();
			} else{
				$(this).parent().find('> ul').slideUp();
			}
		})
		.on('click', '.toggle', function() {
			var event = $(this).is('.closed') ? 'open' : 'close';
			$(this).trigger(event);
		});


	// handle category selection
	$('#wv-discounts-campaign-products').on('click init', 'input[type="checkbox"][name*="categories"]', function(event) {
		var ul = $(this).parent().find('> ul');

		if($(this).prop('checked')) {
			ul
				.find('input[type="checkbox"]')
				.prop('checked', false)
				.each(function() {
					$(this).data('disabled', $(this).prop('disabled')).prop('disabled', true);
				});
		}
		else {
			ul
				.find('input[type="checkbox"]').each(function() {
					$(this).prop('disabled', $(this).data('disabled'));
				});
		}

		var hasChecked = ul.find('input[type="checkbox"]:checked').length > 0;

		if (!hasChecked) {
			$(this).parent().find('> .toggle').trigger('close');
		}
	});

	$('#wv-discounts-campaign-products input[type="checkbox"][name*="categories"]').trigger('init');
});
