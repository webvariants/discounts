<?php

namespace wv\Discounts\Service;

use wv\Payments\Service\Product as Base;
use wv\Payments\Factory as PaymentsFactory;
use wv\Payments\Config as PaymentsConfig;
use wv\Discounts\Factory as DiscountFactory;
use wv\Payments\Model\Product as ProductModel;
use wv\Discounts\Model\Campaign as CampaignModel;
use wv\Payments\Money as PaymentsMoney;

class Product extends Base {
	protected $discountFactory;

	public function __construct(PaymentsFactory $productFactory, PaymentsConfig $productConfig, DiscountFactory $discountFactory) {
		parent::__construct($productFactory, $productConfig);

		$this->discountFactory = $discountFactory;
	}

	public function getOriginalPrice(ProductModel $product) {
		return parent::getBruttoPrice($product);
	}

	public function getBruttoPrice(ProductModel $product) {
		$price    = parent::getBruttoPrice($product);
		$campaign = $this->getCampaign($product);

		if ($campaign) {
			$amount = $campaign->getAmount();
			if ($campaign->isAbsolute()) {
				$price = $price->sub($amount);
			}
			elseif ($campaign->isPercentage()) {
				$price = $price->sub($price->getAmount() * $amount / 100);
			}
		}

		return $price;
	}

	public function getBruttoDiscount(ProductModel $product) {
		$price    = parent::getBruttoPrice($product);
		$campaign = $this->getCampaign($product);
		$discount = 0.0;

		if ($campaign) {
			if ($campaign->isAbsolute()) {
				$discount = $campaign->getAmount();
			}
			elseif ($campaign->isPercentage()) {
				$discount = ($price->getAmount() * $campaign->getAmount() / 100);
			}
		}

		return new PaymentsMoney($discount);
	}

	public function campaignApplies(ProductModel $product, CampaignModel $campaign) {
		$price = parent::getBruttoPrice($product)->getAmount();
		$min   = $campaign->getMin();
		$max   = $campaign->getMax();

		if (!$campaign->isEnabled()) {
			return false;
		}

		if ($campaign->isExpired()) {
			return false;
		}

		if ($price < $min) {
			return false;
		}

		if ($max !== 0 && $price > $max) {
			return false;
		}

		if ($campaign->getType() !== CampaignModel::TYPE_DISCOUNT) {
			return false;
		}

		return true;
	}

	public function getCampaign(ProductModel $product) {
		$article  = $product->getArticle();
		$branch   = $article->getParentTree();
		$branch[] = $article;

		foreach (array_reverse($branch) as $athingKnownToHaveACampaign) {
			$campaignId = $athingKnownToHaveACampaign->getMeta('wv.discounts.campaign');

			if ($campaignId <= 0) {
				continue;
			}

			$campaign = $this->discountFactory->getCampaign($campaignId);

			if ($campaign && $this->campaignApplies($product, $campaign)) {
				return $campaign;
			}
		}

		return null;
	}
}
