<?php
/*
 * Copyright (c) 2016, webvariants GbR, http://www.webvariants.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */

namespace wv\Discounts\Service;

use sly_DB_PDO_Persistence;
use sly_Util_String;
use wv\Discounts\Model\Campaign as CampaignModel;
use wv\Discounts\Model\Coupon as CouponModel;
use wv\Discounts\Service\Coupon as CouponService;

class Campaign {
	protected $persistence;
	protected $couponService;

	public function __construct(sly_DB_PDO_Persistence $persistence, CouponService $couponService) {
		$this->persistence   = $persistence;
		$this->couponService = $couponService;
	}

	public function save(CampaignModel $campaign) {
		$data = array();

		if ($campaign->getId() === CampaignModel::NEW_ID) {
			$data['created'] = $this->getDateTime();
		}

		$data['title']       = $campaign->getTitle();
		$data['uuid']        = $campaign->getUUID();
		$data['attendance']  = $campaign->getAttendance();
		$data['reusable']    = $campaign->getReusable();
		$data['code']        = $campaign->getCode();
		$data['starts']      = $this->getDateTime($campaign->getStartDate());
		$data['expires']     = $this->getDateTime($campaign->getExpiryDate());
		$data['updated']     = $this->getDateTime();
		$data['status']      = $campaign->getStatus();
		$data['computation'] = $campaign->getComputation();
		$data['type']        = $campaign->getType();
		$data['amount']      = $campaign->getAmount();
		$data['min']         = $campaign->getMin();
		$data['max']         = $campaign->getMax();

		$this->persistence->replace('discounts_campaign', $data, array('id' => $campaign->getId()));

		if ($campaign->getId() === CampaignModel::NEW_ID) {
			$campaign->setId($this->persistence->lastId());
		}

		if (!$campaign->isReusable()) {
			$this->addMissingCoupons($campaign);
		}

		return $campaign;
	}

	protected function addMissingCoupons(CampaignModel $campaign) {
		$coupons    = $this->getAllCoupons($campaign);
		$attendance = $campaign->getAttendance();
		$missing    = (int) $attendance - $coupons;

		if ($missing > 0) {
			do {
				$this->couponService->addCoupon($campaign);
			}

			while (--$missing > 0);
		}
	}

	public function delete(CampaignModel $campaign) {
		$this->couponService->deleteByCampaign($campaign);
		$this->persistence->delete('discounts_campaign', array('id' => $campaign->getId()));
	}

	public function export(CampaignModel $campaign) {
		$this->persistence->select('discounts_coupon', 'campaign_id, code, status', array(
			'campaign_id' => $campaign->getId()
		));

		$coupons = array();

		foreach ($this->persistence->all() as $row) {
			$coupons[] = array(
				'campaign_id' => $row['campaign_id'],
				'code'        => $row['code'],
				'status'      => $row['status']
			);
		}

		return $coupons;
	}

	public function getAllCoupons(CampaignModel $campaign) {
		return (int) $this->persistence->magicFetch('discounts_coupon', 'COUNT(id)', array(
			'campaign_id' => $campaign->getId()
		));
	}

	public function getConsumedCoupons(CampaignModel $campaign) {
		return (int) $this->persistence->magicFetch('discounts_coupon', 'COUNT(id)', array(
			'campaign_id' => $campaign->getId(),
			'status'      => CouponModel::STATUS_USED
		));
	}

	public function getActiveCoupons(CampaignModel $campaign) {
		return (int) $this->persistence->magicFetch('discounts_coupon', 'COUNT(id)', array(
			'campaign_id' => $campaign->getId(),
			'status'      => CouponModel::STATUS_ACTIVE
		));
	}

	protected function getDateTime($input = null, $asTimestamp = false) {
		$input = $input ?: time();
		$input = sly_Util_String::isInteger($input) ? (int) $input : strtotime($input);

		return $asTimestamp ? $input : date('Y-m-d H:i:s', $input);
	}
}
