<?php
/*
 * Copyright (c) 2016, webvariants GbR, http://www.webvariants.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */

namespace wv\Discounts\Service;

use wv\Payments\Invoice;
use wv\Payments\Model\Cart as CartModel;
use wv\Payments\Factory as PaymentsFactory;
use wv\Payments\Service\Cart as Base;
use wv\Payments\Service\User as UserService;
use wv\Payments\Service\Country as CountryService;
use wv\Discounts\Factory as DiscountsFactory;
use wv\Discounts\Model\Campaign as CampaignModel;
use wv\Discounts\Model\Coupon as CouponModel;
use wv\Discounts\Service\Coupon as CouponService;
use sly_DB_Persistence;

class Cart extends Base {
	protected $couponService;
	protected $discountsFactory;

	public function __construct(CouponService $couponService, DiscountsFactory $discountsFactory, PaymentsFactory $factory, UserService $service, CountryService $cService, sly_DB_Persistence $db) {
		parent::__construct($factory, $service, $cService, $db);

		$this->couponService    = $couponService;
		$this->discountsFactory = $discountsFactory;
	}

	/**
	 *
	 * @param string $code
	 * @return boolean
	 */
	public function isValidCoupon($code, $user = null) {
		return $this->couponService->isValid($code, $user);
	}

	/**
	 *
	 * @param string $code
	 * @return \wv\Discounts\Model\Campaign
	 */
	public function getCampaignForCode($code) {
		$coupon = $this->discountsFactory->findCouponByCode($code);

		return $this->discountsFactory->getCampaign($coupon->getCampaignId());
	}

	public function getTotalNetto(Invoice $invoice, $includeHandling = false) {
		$netto = parent::getTotalNetto($invoice, $includeHandling);

		foreach ($invoice->getCoupons() as $code => $data) {
			$netto = $this->applyDiscount($code, $netto, $invoice);
		}

		return $netto;
	}

	public function getTotalTaxes(Invoice $invoice, $includeHandling = false) {
		$tax = parent::getTotalTaxes($invoice, $includeHandling);

		foreach ($invoice->getCoupons() as $code => $data) {
			$tax = $this->applyTaxDiscount($code, $tax, $invoice);
		}

		return $tax;
	}

	public function checkout(CartModel $cart) {
		$coupons = $cart->getCoupons();
		foreach (array_keys($coupons) as $code) {
			$campaign = $this->getCampaignForCode($code);
			if (!$campaign || !$this->campaignApplies($cart, $campaign)) {
				unset($coupons[$code]);
			}
		}

		$cart->setData('coupons', $coupons);

		$orderId = parent::checkout($cart);

		foreach (array_keys($coupons) as $code) {
			if ($this->couponService->isValid($code)) {
				$coupon = $this->discountsFactory->findCouponByCode($code);
				$user   = $this->userService->getCurrentUser();

				$coupon->setStatus(CouponModel::STATUS_USED);
				if ($user) {
					$coupon->setUserId($user->getId());
				}
				$this->couponService->update($coupon);
				$this->couponService->log($coupon);

			}
		}

		return $orderId;
	}

	protected function applyTaxDiscount($code, $money, $invoice) {

		if ($this->couponService->isValid($code)) {
			$coupon   = $this->discountsFactory->findCouponByCode($code);
			$campaign = $this->discountsFactory->getCampaign($coupon->getCampaignId());

			if ($campaign && $this->campaignApplies($invoice, $campaign)) {
				$amount  = $campaign->getAmount();

				if ($campaign->isAbsolute()) {
					$taxRate = $this->getMostExpensiveTaxrate($invoice)->getCharge()->getAmount();
					$money   = $money->sub(($amount / (100+$taxRate)) * $taxRate);
				}
				elseif ($campaign->isPercentage()) {
					$money = $money->sub($money->getAmount() * $amount / 100);
				}
			}
		}

		return $money;
	}


	protected function applyDiscount($code, $money, $invoice) {
		if ($this->couponService->isValid($code)) {
			$coupon   = $this->discountsFactory->findCouponByCode($code);
			$campaign = $this->discountsFactory->getCampaign($coupon->getCampaignId());

			if ($campaign && $this->campaignApplies($invoice, $campaign)) {
				$amount = $campaign->getAmount();

				if ($campaign->isAbsolute()) {
					$taxRate = $this->getMostExpensiveTaxrate($invoice)->getCharge()->getAmount();
					$money   = $money->sub($amount / (1+($taxRate/100)));
				}
				elseif ($campaign->isPercentage()) {
					$money = $money->sub($money->getAmount() * $amount / 100);
				}
			}
		}

		return $money;
	}

	public function campaignApplies(Invoice $invoice, CampaignModel $campaign) {
		$price = parent::getTotalNetto($invoice)->getAmount();
		$min   = $campaign->getMin();
		$max   = $campaign->getMax();

		if (!$campaign->isEnabled()) {
			return false;
		}

		if ($campaign->isExpired()) {
			return false;
		}

		if ($price < $min) {
			return false;
		}

		if ($max !== 0 && $price > $max) {
			return false;
		}

		if ($campaign->getType() !== CampaignModel::TYPE_DISCOUNT) {
			return false;
		}

		return true;
	}
}