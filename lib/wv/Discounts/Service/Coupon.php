<?php
/*
 * Copyright (c) 2016, webvariants GbR, http://www.webvariants.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */

namespace wv\Discounts\Service;

use sly_DB_PDO_Persistence;
use wv\Discounts\Factory;
use wv\Discounts\Model\Campaign as CampaignModel;
use wv\Discounts\Model\Coupon as CouponModel;

class Coupon {
	protected $persistence;
	protected $factory;

	public function __construct(sly_DB_PDO_Persistence $persistence, Factory $factory) {
		$this->persistence = $persistence;
		$this->factory     = $factory;
	}

	public function addCoupon(CampaignModel $campaign) {
		$code = $this->generateCode();

		if (!$code) {
			throw new Exception('Es konnten nicht alle Coupons generiert werden. Bitte speichern Sie die Kampagne erneut.');
		}

		$data = array(
			'campaign_id' => $campaign->getId(),
			'code'        => $code,
			'status'      => CouponModel::STATUS_ACTIVE
		);

		$this->persistence->insert('discounts_coupon', $data);
	}

	public function update(CouponModel $coupon) {
		$data = array(
			'user_id' => $coupon->getUserId(),
			'status'  => $coupon->getStatus()
		);

		$this->persistence->update('discounts_coupon', $data, array('id' => $coupon->getId()));
	}

	public function log(CouponModel $coupon) {
		$data = array(
			'campaign_id' => $coupon->getCampaignId(),
			'user_id'     => $coupon->getUserId(),
			'code'        => $coupon->getCode(),
			'created'     => date('Y-m-d H:i:s', time())
		);

		$this->persistence->insert('discounts_coupon_log', $data);
	}

	public function deleteByCampaign(CampaignModel $campaign) {
		$this->persistence->delete('discounts_coupon', array('campaign_id' => $campaign->getId()));
	}

	public function isValid($code, $user = null) {
		$coupon = $this->factory->findCouponByCode($code, $user);
		return $coupon !== null && ($coupon->isActive());
	}

	public function generateCode($length = 10, $upper = true, $num = true, $lower = false) {
		$tries = 10;

		do {
			$code = $this->codeGen($length, $upper, $num, $lower);

			if (!$this->isValid($code)) {
				return $code;
			}
		}

		while (--$tries > 0);
	}

	protected function codeGen($length = 10, $upper = true, $num = true, $lower = false) {
		$chars = '';

		if ($lower) $chars .= 'abcdefghijkmnpqrstuvwxyz';
		if ($upper) $chars .= 'ABCDEFGHIJKLMNPQRSTUVWXYZ';
		if ($num)   $chars .= '1234567890';

		$len = strlen($chars) - 1;
		$res = '';

		if ($length > 32) {
			$length = 32;
		}

		while ($length-- > 0) {
			$res .= $chars[mt_rand(0, $len)];
		}

		return $res;
	}
}
