<?php
/*
 * Copyright (c) 2016, webvariants GbR, http://www.webvariants.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */

namespace wv\Discounts;

use sly_Model_User;
use sly_DB_PDO_Persistence;
use InvalidArgumentException;
use wv\Discounts\Model\Campaign       as CampaignModel;
use wv\Discounts\Model\Coupon         as CouponModel;
use wv\Discounts\Model\CouponReusable as CouponReusableModel;

class Factory {
	protected $persistence;

	public function __construct(sly_DB_PDO_Persistence $persistence) {
		$this->persistence = $persistence;
	}

	public function getCampaigns($where = '1', $offset = null, $limit = null, $orderBy = 'created DESC') {
		$this->persistence->select('discounts_campaign', 'id', $where, null, $orderBy, $offset, $limit);

		$campaigns = array();

		foreach ($this->persistence->all() as $row) {
			$campaigns[] = $this->getCampaign($row['id']);
		}

		return $campaigns;
	}

	public function getCampaign($id) {
		$data = $this->persistence->magicFetch('discounts_campaign', '*', array('id' => $id));

		if ($data) {
			$id          = $data['id'];
			$title       = $data['title'];
			$uuid        = $data['uuid'];
			$attendance  = $data['attendance'];
			$reusable    = $data['reusable' ];
			$code        = $data['code' ];
			$created     = strtotime($data['created']);
			$updated     = strtotime($data['updated']);
			$starts      = strtotime($data['starts']);
			$expires     = strtotime($data['expires']);
			$status      = $data['status'];
			$computation = $data['computation'];
			$type        = $data['type'];
			$amount      = $data['amount'];
			$min         = $data['min'];
			$max         = $data['max'];

			$campaign = new CampaignModel($id, $created, $updated);
			$campaign->setTitle($title);
			$campaign->setUUID($uuid);
			$campaign->setAttendance($attendance);
			$campaign->setReusable($reusable);
			$campaign->setCode($code);
			$campaign->setStartDate($starts);
			$campaign->setExpiryDate($expires);
			$campaign->setStatus($status);
			$campaign->setComputation($computation);
			$campaign->setType($type);
			$campaign->setAmount($amount);
			$campaign->setMin($min);
			$campaign->setMax($max);

			return $campaign;
		}
	}

	public function getCoupons(CampaignModel $campaign) {
		$this->persistence->select('discounts_coupon', 'id', array('campaign_id' => $campaign->getId()));

		$coupons = array();

		foreach ($this->persistence->all() as $row) {
			$coupons[] = $this->getCoupon($row['id']);
		}

		return $coupons;
	}

	public function getCoupon($id) {
		$couponData = $this->persistence->magicFetch('discounts_coupon', '*', array('id' => $id));

		if (!$couponData) {
			throw new InvalidArgumentException('Invalid coupon ID '.$id.' given.');
		}

		$id         = $couponData['id'];
		$campaignId = $couponData['campaign_id'];
		$userId     = $couponData['user_id'];
		$code       = $couponData['code'];
		$status     = $couponData['status'];

		return new CouponModel($id, $campaignId, $userId, $code, $status);
	}

	public function getReusableCoupon($id, sly_Model_User $user = null) {
		$campaignData = $this->persistence->magicFetch('discounts_campaign', 'id, code', array('id' => $id));

		if (!$campaignData) {
			throw new InvalidArgumentException('Invalid coupon ID '.$id.' given.');
		}

		if ($user) {
			$campaignData['user_id'] = $user->getId();

			$status = $this->persistence->magicFetch('discounts_coupon_log', 'COUNT(id)', array(
				'code'    => $campaignData['code'],
				'user_id' => $campaignData['user_id']
			));

			$campaignData['status'] = (int) $status === 0 ? CouponModel::STATUS_ACTIVE : CouponModel::STATUS_USED;
		}
		else {
			$campaignData['user_id'] = null;
			$campaignData['status']  = CouponModel::STATUS_ACTIVE;
		}

		$id         = 0;
		$campaignId = $campaignData['id'];
		$userId     = $campaignData['user_id'];
		$code       = $campaignData['code'];
		$status     = $campaignData['status'];

		return new CouponReusableModel($id, $campaignId, $userId, $code, $status);
	}

	public function findCouponsByUser(sly_Model_User $user) {
		$this->persistence->select('discounts_coupon', 'id', array('user_id' => $user->getId(), 'status' => CouponModel::STATUS_USED));

		$coupons = array();

		foreach ($this->persistence->all() as $row) {
			$coupons[] = $this->getCoupon($row['id']);
		}

		$this->persistence->select('discounts_coupon_log', 'campaign_id', array('user_id' => $user->getId()));

		foreach ($this->persistence->all() as $row) {
			$coupons[] = $this->getReusableCoupon($row['campaign_id'], $user);
		}

		return $coupons;
	}

	public function findCouponByCode($code, sly_Model_User $user = null) {
		$coupon = $this->persistence->fetch('discounts_coupon', 'id', array('code' => $code));

		if ($coupon) {
			return $this->getCoupon($coupon['id']);
		}

		$campaign = $this->persistence->fetch('discounts_campaign', 'id', array('code' => $code));

		if ($campaign) {
			return $this->getReusableCoupon($campaign['id'], $user);
		}

		return null;
	}
}
