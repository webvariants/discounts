<?php
/*
 * Copyright (c) 2016, webvariants GbR, http://www.webvariants.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */

namespace wv\Discounts\Model;

use wv\Payments\Model\Cart as Base;
use \wv\Payments\Invoice;
use sly_Core;

class Cart extends Base implements Invoice {
	protected $coupons;

	public function addCoupon($code, $user = null) {
		$this->getCoupons();
		$valid = $this->getService()->isValidCoupon($code, $user);

		if ($valid) {
			$campaign = $this->getService()->getCampaignForCode($code);
			$valid    = $campaign && $this->getService()->campaignApplies($this, $campaign);
		}

		if ($valid) {
			$this->coupons[$code] = array(
										'absolute' => $campaign->isAbsolute(),
										'amount'   => $campaign->getAmount()
									);
		}

		return $valid;
	}

	public function removeCoupon($code) {
		$this->getCoupons();

		if(isset($this->coupons[$code])) {
			unset($this->coupons[$code]);
		}
	}

	public function getCoupons() {
		if ($this->coupons === null) {
			$this->coupons = $this->getData('coupons', array());
		}

		return $this->coupons;
	}

	public function update() {
		if ($this->coupons !== null) {
			$this->setData('coupons', $this->coupons);
		}

		return parent::update();
	}

	/**
	 *
	 * @return \wv\Discounts\Service\Cart
	 */
	protected function getService() {
		return sly_Core::getContainer()->get('wv-discounts-service-cart');
	}
}