<?php
/*
 * Copyright (c) 2016, webvariants GbR, http://www.webvariants.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */

namespace wv\Discounts\Model;

class Coupon {
	protected $id;
	protected $campaignId;
	protected $userId;
	protected $code;
	protected $status;

	const STATUS_ACTIVE = 0;
	const STATUS_USED   = 1;

	public function __construct($id, $campaignId, $userId, $code, $status) {
		$this->id         = (int) $id;
		$this->campaignId = (int) $campaignId;
		$this->userId     = (int) $userId;
		$this->code       = $code;
		$this->status     = (int) $status;
	}

	public function getId() {
		return $this->id;
	}

	public function getCampaignId() {
		return $this->campaignId;
	}

	public function getUserId() {
		return $this->userId;
	}

	public function getCode() {
		return $this->code;
	}

	public function getStatus() {
		return $this->status;
	}

	public function isActive() {
		return $this->status === self::STATUS_ACTIVE;
	}

	public function isReusable() {
		return false;
	}

	public function isUsed() {
		return $this->status === self::STATUS_USED;
	}

	public function setUserId($userId) {
		$this->userId = (int) $userId;
	}

	public function setCode($code) {
		$this->code = $code;
	}

	public function setStatus($status) {
		$this->status = (int) $status;
	}
}
