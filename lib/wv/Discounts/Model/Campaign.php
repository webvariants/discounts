<?php
/*
 * Copyright (c) 2016, webvariants GbR, http://www.webvariants.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */

namespace wv\Discounts\Model;

class Campaign {
	protected $id;
	protected $title;
	protected $uuid;
	protected $attendance;
	protected $reusable;
	protected $code;
	protected $created;
	protected $updated;
	protected $starts;
	protected $expires;
	protected $status;
	protected $computation;
	protected $type;
	protected $amount;
	protected $min;
	protected $max;

	const NEW_ID = 0;

	const STATUS_DISABLED  = 0;
	const STATUS_ENABLED   = 1;
	const STATUS_COMPLETED = 2;

	const COMPUTATION_PERCENTAGE = 1;
	const COMPUTATION_ABSOLUTE   = 2;

	const TYPE_DISCOUNT = 1;
	const TYPE_BONUS    = 2;

	const IS_REUSABLE   = 1;

	public function __construct($id, $created, $updated) {
		$this->id      = (int) $id;
		$this->created = $created;
		$this->updated = $updated;
	}

	public function getId() {
		return $this->id;
	}

	public function getTitle() {
		return $this->title;
	}

	public function getUUID() {
		return $this->uuid;
	}

	public function getAttendance() {
		return $this->attendance;
	}

	public function getReusable() {
		return $this->reusable;
	}

	public function getCode() {
		return $this->code;
	}

	public function getCreateDate() {
		return $this->created;
	}

	public function getUpdateDate() {
		return $this->updated;
	}

	public function getStartDate() {
		return $this->starts;
	}

	public function getExpiryDate() {
		return $this->expires;
	}

	public function getStatus() {
		return $this->status;
	}

	public function getComputation() {
		return $this->computation;
	}

	public function getType() {
		return $this->type;
	}

	public function isAbsolute() {
		return $this->computation === self::COMPUTATION_ABSOLUTE;
	}

	public function isPercentage() {
		return $this->computation === self::COMPUTATION_PERCENTAGE;
	}

	public function isDiscount() {
		return $this->type === self::TYPE_DISCOUNT;
	}

	public function isBonus() {
		return $this->type === self::TYPE_BONUS;
	}

	public function getAmount() {
		return $this->amount;
	}

	public function getMin() {
		return $this->min;
	}

	public function getMax() {
		return $this->max;
	}

	public function isDisabled() {
		return $this->status === self::STATUS_DISABLED;
	}

	public function isEnabled() {
		return $this->status === self::STATUS_ENABLED;
	}

	public function isCompleted() {
		return $this->status === self::STATUS_COMPLETED;
	}

	public function isExpired($time = null) {
		$time = is_null($time) ? time() : $time;

		return $this->starts > $time || $this->expires < $time;
	}

	public function isReusable() {
		return $this->reusable === self::IS_REUSABLE;
	}

	public function setId($id) {
		$this->id = (int) $id;
	}

	public function setTitle($title) {
		$this->title = $title;
	}

	public function setUUID($uuid) {
		$this->uuid = $uuid;
	}

	public function setAttendance($attendance) {
		$this->attendance = (int) $attendance;
	}

	public function setReusable($reusable) {
		$this->reusable = (int) $reusable;
	}

	public function setCode($code) {
		$this->code = $code;
	}

	public function setStartDate($starts) {
		$this->starts = $starts;
	}

	public function setExpiryDate($expires) {
		$this->expires = $expires;
	}

	public function setStatus($status) {
		$this->status = (int) $status;
	}

	public function setComputation($computation) {
		$this->computation = (int) $computation;
	}

	public function setType($type) {
		$this->type = (int) $type;
	}

	public function setAmount($amount) {
		$this->amount = (int) $amount;
	}

	public function setMin($min) {
		$this->min = (int) $min;
	}

	public function setMax($max) {
		$this->max = (int) $max;
	}
}
