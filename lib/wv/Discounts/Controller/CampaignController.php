<?php
/*
 * Copyright (c) 2016, webvariants GbR, http://www.webvariants.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */

namespace wv\Discounts\Controller;

use sly_Response;
use sly_Controller_Backend;
use sly_Controller_Interface;
use sly_Helper_Message;
use sly_Util_Language;
use sly\Assets\Util as AssetUtil;
use wv\Discounts\Model\Campaign as CampaignModel;
use wv\metainfo\Metainfo;



class CampaignController extends sly_Controller_Backend implements sly_Controller_Interface {
	const ADD_METHOD    = 'add';
	const EDIT_METHOD   = 'edit';
	const DELETE_METHOD = 'delete';

	public function indexAction() {
		$this->pageHeader();

		$container = $this->getContainer();
		$service   = $container['wv-discounts-service-campaign'];
		$factory   = $container['wv-discounts-factory'];

		$data = array();
		$data['service']   = $service;

		print sly_Helper_Message::renderFlashMessage();

		$campaigns = $factory->getCampaigns();
		$coupon    = array();
		$product   = array();

		foreach ($campaigns as $c) {
			if ($c->getAttendance() > 0 || $c->isReusable()) {
				$coupon[] = $c;
			}else {
				$product[] = $c;
			}
		}

		print $this->render('coupon_campaigns.phtml', array_merge($data, array('campaigns' => $coupon)));
		print $this->render('product_campaigns.phtml', array_merge($data, array('campaigns' => $product)));
	}

	public function addAction() {
		$this->campaignView(self::ADD_METHOD);
	}

	public function editAction() {
		$this->campaignView(self::EDIT_METHOD);
	}

	protected function campaignView($method) {
		$this->pageHeader();

		$type      = $this->getRequest()->get('type', 'string');
		$container = $this->getContainer();
		$request   = $this->getRequest();
		$factory   = $container['wv-discounts-factory'];
		$campaign  = $factory->getCampaign($request->get('id', 'int'));

		if (!$campaign) {
			$campaign = new CampaignModel(0, null, null);
			$campaign->setAttendance(1);
			$campaign->setComputation(CampaignModel::COMPUTATION_PERCENTAGE);
			$campaign->setType(CampaignModel::TYPE_DISCOUNT);
			$campaign->setStartDate(time());
			$campaign->setExpiryDate(time());
			$campaign->setAmount(0);
			$campaign->setMin(0);
			$campaign->setMax(0);
		}

		$data = array(
			'method'   => $method,
			'campaign' => $campaign,
			'type'     => $type
		);

		if ($type === 'product') {
			$selectedCategories = ($campaign->getId() !== 0) ? Metainfo::getCategoriesWithMetaData('wv.discounts.campaign', false, $campaign->getId()) : array();
			array_walk($selectedCategories, function(&$category) {
				$category = $category->getId();
			});

			$data['products'] = $container['wv-payments-factory']->getProducts(\sly_Core::getCurrentClang(), 'path ASC, catpos ASC, pos ASC', \sly_Service_Article::FIND_REVISION_BEST);
			$data['selectedCategories'] = $selectedCategories;
		}

		$this->render('edit_campaign.phtml', $data, false);
	}

	public function campaignAction() {
		$campaign = $this->getCampaignFromRequest();

		if (!$campaign) {
			$campaign = new CampaignModel(0, null, null);
		}

		$request   = $this->getRequest();
		$container = $this->getContainer();
		$service   = $container['wv-discounts-service-campaign'];
		$flash     = $container['sly-flash-message'];

		$title       = $request->post('title', 'string');
		$uuid        = $request->post('uuid', 'string', '');
		$attendance  = $request->post('attendance', 'int');
		$reusable    = $request->post('reusable', 'int');
		$code        = $request->post('code', 'string', '');
		$status      = $request->post('status', 'int');
		$starts      = $request->post('starts', 'string');
		$expires     = $request->post('expires', 'string');
		$computation = $request->post('computation', 'int');
		$type        = $request->post('type', 'int');
		$amount      = $request->post('amount', 'int');
		$min         = $request->post('min', 'int');
		$max         = $request->post('max', 'int');
		$products    = $request->postArray('products', 'boolean', array());
		$categories  = $request->postArray('categories', 'boolean', array());

		try {
			$campaign->setTitle($title);
			$campaign->setUUID($uuid);
			$campaign->setStatus($status);
			$campaign->setAttendance($attendance);
			$campaign->setReusable($reusable);
			$campaign->setCode($code);
			$campaign->setStartDate($starts);
			$campaign->setExpiryDate($expires);
			$campaign->setComputation($computation);
			$campaign->setType($type);
			$campaign->setAmount($amount);
			$campaign->setMin($min);
			$campaign->setMax($max);

			$service->save($campaign);

			$metavalueService   = $container['metainfo-service-value'];
			$selectedProducts   = Metainfo::getArticlesWithMetaData('wv.discounts.campaign', false, $campaign->getId());
			$selectedCategories = Metainfo::getCategoriesWithMetaData('wv.discounts.campaign', false, $campaign->getId());
			array_walk($selectedProducts,   function(&$item){$item = $item->getId();});
			array_walk($selectedCategories, function(&$item){$item = $item->getId();});

			foreach (sly_Util_Language::findAll(true) as $clangID) {

				foreach ($products as $productID => $value) {
					$product = \sly_Util_Article::findById($productID, $clangID, \sly_Service_Article::FIND_REVISION_BEST);

					if ($value) {
						$metavalueService->updateArticle($product, 'wv.discounts.campaign', $campaign->getId(), false);
					}
					elseif (in_array($productID, $selectedProducts)) {
						$metavalueService->updateArticle($product, 'wv.discounts.campaign', null, false);
					}
				}


				foreach ($categories as $categoryID => $value)  {
					$category = \sly_Util_Category::findById($categoryID, $clangID);

					if ($value) {
						$metavalueService->updateCategory($category, 'wv.discounts.campaign', $campaign->getId(), false);
					}
					elseif (in_array($categoryID, $selectedCategories)) {
						$metavalueService->updateCategory($category, 'wv.discounts.campaign', null, false);
					}
				}
			}
			Metainfo::clearDataCache();

			$flash->appendInfo('Kampagne bearbeitet');
		}
		catch (Exception $e) {
			$flash->appendWarning($e->getMessage());
			return $this->campaignAction('id', $campaign->getId());
		}

		return $this->redirectResponse();
	}

	public function deleteAction() {
		$container = $this->getContainer();
		$service   = $container['wv-discounts-service-campaign'];
		$campaign  = $this->getCampaignFromRequest();
		$flash     = $container['sly-flash-message'];

		if (!$campaign) {
			$flash->appendInfo('Kampagne nicht gefunden!');
			$this->redirectResponse();
		}

		try {
			$service->delete($campaign);

			$metavalueService = $container['metainfo-service-value'];
			$articles         = Metainfo::getArticlesWithMetaData('wv.discounts.campaign', false, $campaign->getId());
			$categories       = Metainfo::getCategoriesWithMetaData('wv.discounts.campaign', false, $campaign->getId());

			foreach($articles as $article) {
				$metavalueService->updateArticle($article, 'wv.discounts.campaign', null, false);
			}
			foreach($categories as $category) {
				$metavalueService->updateCategory($category, 'wv.discounts.campaign', null, false);
			}

			Metainfo::clearDataCache();

			$flash->appendInfo('Kampagne gelöscht');
		}
		catch (Exception $e) {
			$flash->appendWarning($e->getMessage());
			return $this->indexAction();
		}

		return $this->redirectResponse();
	}

	public function exportAction() {
		$campaign = $this->getCampaignFromRequest();

		if (!$campaign) {
			$this->redirectResponse();
		}

		$container = $this->getContainer();
		$service   = $container['wv-discounts-service-campaign'];
		$flash     = $container['sly-flash-message'];

		try {
			$handle = fopen('php://output', 'wb');
			$export = $service->export($campaign);

			ob_start();

			foreach ($export as $data) {
				fputcsv($handle, $data);
			}

			$content = ob_get_clean();

			fclose($handle);

			$response = new sly_Response($content);
			$response->setContentType('text/csv', 'UTF-8');
			$response->setHeader('Content-Disposition', 'attachment; filename=coupons.csv');

			return $response;
		}
		catch (Exception $e) {
			$flash->appendWarning($e->getMessage());
			return $this->indexAction();
		}
	}

	protected function getCampaignFromRequest() {
		$container = $this->getContainer();
		$request   = $this->getRequest();
		$factory   = $container['wv-discounts-factory'];
		$id        = $request->request('id', 'string');

		try {
			return $factory->getCampaign($id);
		}
		catch (Exception $e) {
			$container['sly-flash-message']->appendWarning($e->getMessage());
		}
	}

	protected function getViewFolder() {
		return $this->container['wv-discounts-viewfolder'];
	}

	protected function pageHeader() {
		$layout = $this->container['sly-layout'];
		$layout->addCSSFile(AssetUtil::addOnUri('webvariants/discounts', 'less/backend.less'));
		$layout->addJavaScriptFile(AssetUtil::addOnUri('webvariants/discounts', 'js/backend.js'));
		$layout->pageHeader(t('wv.discounts.discounts'));
	}

	public function checkPermission($action) {
		$user = $this->getCurrentUser();

		return $user && ($user->isAdmin() || $user->hasPermission('pages', 'discounts'));
	}
}
