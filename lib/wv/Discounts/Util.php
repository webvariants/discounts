<?php
/*
 * Copyright (c) 2016, webvariants GbR, http://www.webvariants.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */

namespace wv\Discounts;

use sly_Container;
use sly_Event_IDispatcher;
use wv\Discounts\Service;

class Util {
	public static function boot(sly_Container $container, $dir) {
		$container['wv-discounts-config'] = $container->share(function($container) {
			$config = $container['sly-config'];

			return new Config($config);
		});

		$container['wv-discounts-factory'] = $container->share(function($container) {
			$persistence = $container['sly-persistence'];

			return new Factory($persistence);
		});

		$container['wv-discounts-service-coupon'] = $container->share(function($container) {
			$persistence = $container['sly-persistence'];
			$factory     = $container['wv-discounts-factory'];

			return new Service\Coupon($persistence, $factory);
		});

		$container['wv-discounts-service-campaign'] = $container->share(function($container) {
			$persistence = $container['sly-persistence'];
			$service     = $container['wv-discounts-service-coupon'];

			return new Service\Campaign($persistence, $service);
		});

		$container['wv-discounts-listeners'] = $container->share(function() {
			return new Listeners();
		});

		$container['sly-backend-controller-campaigns'] = $container->share(function() {
			return new Controller\CampaignController();
		});

		$container->getI18N()->appendFile($dir.DIRECTORY_SEPARATOR.'i18n');

		$container['wv-discounts-viewfolder'] = $dir.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR;

		$container['wv-discounts-service-cart'] = $container->share(function($container) {
			$paymentsFactory  = $container['wv-payments-factory'];
			$userService      = $container['wv-payments-service-user'];
			$countryService   = $container['wv-payments-service-country'];
			$discountsFactory = $container['wv-discounts-factory'];
			$couponService    = $container['wv-discounts-service-coupon'];
			$db               = $container['sly-persistence'];

			return new Service\Cart($couponService, $discountsFactory, $paymentsFactory, $userService, $countryService, $db);
		});

		$container['wv-discounts-event-orderinformationlistener'] = $container->share(function($container) {
			return new Event\OrderInformationListener($container['wv-discounts-factory']);
		});

		self::extendPayments($container);
		self::initListeners($container['sly-dispatcher']);
	}

	public static function extendPayments(sly_Container $container) {
		// extend product service
		$container['wv-payments-service-product'] = $container->share(function($container) {
			return new Service\Product($container['wv-payments-factory'], $container['wv-payments-config'], $container['wv-discounts-factory']);
		});


		$productTypes = $container['wv-payments-config']->getArticleTypes();

		// add hidden metainfo for campaign id
		$container['sly-config']->set('metainfo/articles', array(
			'wv.discounts.campaign' => array(
				'title' => '',
				'datatype' => 'number',
				'backend' => false,
				'types' => $productTypes
			)
		));

		// add hidden metainfo for campaign id to categories
		$container['sly-config']->set('metainfo/categories', array(
			'wv.discounts.campaign' => array(
				'title' => '',
				'datatype' => 'number',
				'backend' => false,
				'types' => $productTypes
			)
		));

		$container['wv-payments-cart'] = $container->share(function($container) {
			$storage = new \wv\Payments\CartStorage\Session($container['sly-session']);

			return new Model\Cart($storage);
		});
	}

	public static function initListeners(sly_Event_IDispatcher $dispatcher) {
		$dispatcher->addListener('SLY_BACKEND_NAVIGATION_INIT', array('%wv-discounts-listeners%', 'initBackendNavigation'));
		$dispatcher->addListener('WV_PAYMENTS_ORDER_INFORMATION', array('%wv-discounts-event-orderinformationlistener%', 'getCouponRow'));
	}
}
