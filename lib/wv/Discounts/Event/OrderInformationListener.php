<?php

namespace wv\Discounts\Event;

use wv\Discounts\Factory;

class OrderInformationListener {
	protected $factory;

	public function __construct(Factory $factory) {
		$this->factory = $factory;
	}


	public function getCouponRow($subject, $params) {
		$order   = $params['order'];
		$data    = $order->getData();
		$coupons = isset($data['coupons']) ? $data['coupons'] : array();

		foreach ($coupons as $code => $data) {
			if ($data['absolute']) {
				$m = new \wv\Payments\Money($data['amount']);
				$value = $m->format();
			}
			else {
				$value = $data['amount'].'%';
			}

			$subject .='<tr><td></td>';
			$subject .='<td>'.t('wv.discounts.coupon').'</td>';
			$subject .='<td class="right" colspan="4">'.t('wv.discounts.coupon-grants', $code, $value).'</td>';
			$subject .='</tr>';
		}

		return $subject;
	}
}

