<?php
/*
 * Copyright (c) 2016, webvariants GbR, http://www.webvariants.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */

namespace wv\Discounts;

use sly_Configuration;

class Config {
	protected $config;

	public function __construct(sly_Configuration $config) {
		$this->config = $config;
	}

	public function getConfig() {
		return $this->config->get('wv/discounts', array());
	}
}
