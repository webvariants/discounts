<?php
/*
 * Copyright (c) 2016, webvariants GbR, http://www.webvariants.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */

namespace wv\Discounts;

use sly_Layout_Navigation_Backend;

class Listeners {
	public function initBackendNavigation(sly_Layout_Navigation_Backend $nav, array $params) {
		$user = $params['user'];

		if ($user && ($user->isAdmin() || $user->hasPermission('pages', 'discounts'))) {
			$nav->addPage('addon', 'campaigns', t('wv.discounts.discounts'));
		}
	}
}
