DROP TABLE IF EXISTS `%TABLE_PREFIX%discounts_campaign`;
DROP TABLE IF EXISTS `%TABLE_PREFIX%discounts_coupon`;
DROP TABLE IF EXISTS `%TABLE_PREFIX%discounts_coupon_log`;

CREATE TABLE `%TABLE_PREFIX%discounts_campaign` (
	`id`          INT UNSIGNED        NOT NULL AUTO_INCREMENT,
	`title`       VARCHAR(32)         NOT NULL,
	`uuid`        VARCHAR(255)        NOT NULL DEFAULT '',
	`attendance`  MEDIUMINT UNSIGNED  NOT NULL,
	`reusable`    TINYINT(1) UNSIGNED NOT NULL,
	`code`        VARBINARY(32)       NOT NULL,
	`created`     DATETIME            NOT NULL,
	`updated`     DATETIME            NOT NULL,
	`starts`      DATETIME            NOT NULL,
	`expires`     DATETIME            NOT NULL,
	`status`      TINYINT(1) UNSIGNED NOT NULL,
	`computation` TINYINT(1) UNSIGNED NOT NULL,
	`type`        TINYINT(1) UNSIGNED NOT NULL,
	`amount`      DECIMAL(14,3)       NOT NULL,
	`min`         MEDIUMINT UNSIGNED  NOT NULL,
	`max`         MEDIUMINT UNSIGNED  NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `%TABLE_PREFIX%discounts_coupon` (
	`id`          INT UNSIGNED        NOT NULL AUTO_INCREMENT,
	`campaign_id` INT UNSIGNED        NOT NULL,
	`user_id`     INT UNSIGNED        NOT NULL DEFAULT '0',
	`code`        VARBINARY(32)       NOT NULL,
	`status`      TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `%TABLE_PREFIX%discounts_coupon_log` (
	`id`          INT UNSIGNED  NOT NULL AUTO_INCREMENT,
	`campaign_id` INT UNSIGNED  NOT NULL,
	`user_id`     INT UNSIGNED  NOT NULL DEFAULT '0',
	`code`        VARBINARY(32) NOT NULL,
	`created`     DATETIME      NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
